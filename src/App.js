import logo from './logo.svg';
import { useEffect, useState } from 'react';
import './App.css';
import SurveyCreatorWidget from './Survey'

function App() {

  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/users')
    .then(res => res.json())
    .then(jsondata => setUsers(jsondata))
    .catch(err => console.log(err));
  },[]);

  
  
  return (
    <div className="App">
      <SurveyCreatorWidget />

      
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {(users.length > 0) ? 
          users.map((user, index) => (
            <div key={index}>
              <span>{user.user_id}</span>
              <span>. </span>
              <span>{user.firstname}</span>
            </div>
          ))
          : "1"
        }
        
      </header> */}
    </div>
  );
}

export default App;
