import 'survey-core/defaultV2.min.css';
import './Survey.css';
import { Model } from 'survey-core';
import { Survey } from 'survey-react-ui';
import { useCallback } from 'react';
import CompletedPage from './CompletedPage';


const SURVEY_ID = 1;

const surveyJson = {
    pages: [{
        elements: [{
            name: "satisfaction-score",
            title: "How would you describe your experience with our product?",
            type: "rating",
            rateValues: [
                "Fully satisfying",
                "Generally satisfying",
                "Neutral",
                "Rather unsatisfying"
            ],
            isRequired: false,
        }]
    }, {
        elements: [{
            name: "nps-score",
            title: "On a scale of zero to ten, how likely are you to recommend our product to a friend or colleague?",
            type: "rating",
            rateMin: 0,
            rateMax: 10,
            isRequired: true,
        },{
            name: "satisfaction-score-bariers",
            title: "How would you describe your experience with our product?",
            type: "rating",
            "rateValues": [
                "Fully satisfying",
                "Generally satisfying",
                "Neutral",
                "Rather unsatisfying"
            ],
            isRequired: true,
            
        } ],
    },{
        elements: [
            {
                name: "satisfactionScoreBariers2",
                title: "How would you describe your experience with our product?",
                type: "rating",
                "rateValues": [
                    "Fully satisfying2",
                    "Generally satisfying2",
                    "Neutral2",
                    "Rather unsatisfying2"
                ],
                isRequired: true,
                visibleIf: "{satisfaction-score-bariers} = 'Rather unsatisfying'"
            }
        ]
    }, {
        elements: [{
            name: "disappointing-experience",
            title: "Please let us know why you had such a disappointing experience with our product",
            type: "comment",
            isRequired: true,
        }],
    }],
    // pageNextText: "Далее", 
    // pagePrevText: "Назад",
    goNextPageAutomatic: true,
    completeText: "Ответить",
    showNavigationButtons: "False",
    /* completedHtml: 'Вы умничка'*/
};

export default function SurveyCreatorWidget() {
    // surveyJson.completedHTML = "Финиш";
    const survey = new Model(surveyJson);

    const alertResults = useCallback((sender) => {
        const results = JSON.stringify(sender.data);
        alert(results);
        return <CompletedPage />;
    }, []);
    

    const hideButton = useCallback((sender) => {
        if(sender.isLastPage == true){
            sender.showNavigationButtons = true;
            sender.showPrevButton = false;
        }
    }, []);

    survey.onCurrentPageChanged.add(hideButton);
    
    survey.onComplete.add(alertResults);
    
    return <Survey model={survey} />;
}


function saveSurveyResults(url, json) {
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: JSON.stringify(json)
    })
    .then(response => {
      if (response.ok) {
        // Handle success
      } else {
        // Handle error
      }
    })
    .catch(error => {
      // Handle error
    });
}